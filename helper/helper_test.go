package helper

import (
	"context"
	"testing"

	"github.com/rs/zerolog/log"
)

func TestRecover(t *testing.T) {
	t.Run("normal test", func(t *testing.T) {
		defer Recover(context.Background())
		panic("???")
	})

	t.Run("have zerolog ctx test", func(t *testing.T) {
		ctx := context.Background()
		logger := log.Logger.With().Bool("test", true).Logger()
		ctx = logger.WithContext(ctx)
		defer Recover(ctx)
		panic("???")
	})
}
