package dao

{{ if .SkipImport }}
{{ .ImportStr }}
{{ else }}
import (
	"reflect"
	"time"

	common "gitlab.com/howmay/gopher/common/v1"
	"gorm.io/gorm"
)
{{ end }}
{{ if .SkipWhereOption }}
{{ .WhereOptionStruct }}
{{ else }}
type {{ .ModelName }}WhereOption struct {
	{{ .ModelName }}      
	Pagination *common.Pagination 
	BaseWhere  *common.BaseWhere  
	Sorting    *common.Sorting  
}
{{ end }}

func New{{ .ModelName }}WhereOption() *{{ .ModelName }}WhereOption {
	return &{{ .ModelName }}WhereOption{}
}
{{ if .SkipWhere }}
{{ .WhereFunc }}
{{ else }}
func (where *{{ .ModelName }}WhereOption) Where(db *gorm.DB) *gorm.DB {
	if where.BaseWhere != nil {
		db = db.Scopes(where.BaseWhere.Where)
	}
	return db
}
{{ end }}
{{range .StructFields }}
func (where *{{ $.ModelName }}WhereOption) {{  .Name }}(in {{ .Type }}) *{{ $.ModelName }}WhereOption {
	where.{{ $.ModelName }}.{{  .Name }} = in
	return where
}
{{end}}

func (where *{{ .ModelName }}WhereOption) Page(db *gorm.DB) *gorm.DB {
	if where.Pagination != nil {
		return where.Pagination.LimitAndOffset(db)
	}
	return db
}

func (where *{{ .ModelName }}WhereOption) Sort(db *gorm.DB) *gorm.DB {
	if where.Sorting != nil {
		where.Sorting.Sort(db)
	}
	return db
}

func (where *{{ .ModelName }}WhereOption) IsEmptyWhereOpt() bool {
	return reflect.DeepEqual(where.{{ .ModelName }}, {{ .ModelName }}{})
}

func (where *{{ .ModelName }}WhereOption) TableName() string {
	return where.{{ .ModelName }}.TableName()
}

{{ if .SkipPreload }}
{{ .PreloadFunc }}
{{ else }}
func (where *{{ .ModelName }}WhereOption) Preload(db *gorm.DB) *gorm.DB {
	return db
}
{{ end }}
func (where *{{ .ModelName }}WhereOption) WithoutCount() bool {
	return where.Pagination.WithCount
}

type {{.ModelName}}UpdateColumn struct {
	Values map[string]interface{}
}

func New{{ .ModelName }}UpdateColumn() *{{ .ModelName }}UpdateColumn {
	return &{{ .ModelName }}UpdateColumn{
		Values: make(map[string]interface{}),
	}
}

func (col *{{.ModelName}}UpdateColumn) Columns() interface{} {
	return col
}

{{range .StructFields }}
func (col *{{ $.ModelName }}UpdateColumn) {{ .Name }}(in {{ .Type }}) {
	col.Values["{{ .NameSnake }}"] = in
}
{{end}}