package dao

import (
	"time"

	common "gitlab.com/howmay/gopher/common/v1"
	"gorm.io/gorm"
)

type (
	ShopDomainType uint8
)

const (
	ShopDomainSystemDefault ShopDomainType = 1
)

// ShopDomain 商店資訊
type ShopDomain struct {
	ID        uint64 `gorm:"primarykey"`
	CreatedAt time.Time
	UpdatedAt time.Time
	DeletedAt gorm.DeletedAt `gorm:"index"`

	ShopID     uint64
	DomainName string
	Type       ShopDomainType `gorm:"default:1"`
	IsEnabled  common.YesNo   `gorm:"default:1"`
}

func (ShopDomain) TableName() string {
	return "shop_domain"
}

func (result *ShopDomain) ConvertFromCreate(in *apiv1.CreateShopDomainReq) *ShopDomain {
	if result == nil {
		result = &ShopDomain{}
	}

	result.ShopID = in.ShopId
	result.DomainName = in.DomainName
	result.Type = ShopDomainType(in.Type)
	result.IsEnabled = common.YesNo(in.IsEnabled)

	return result
}

func (result *ShopDomain) ConvertToProto(out *dtov1.ShopDomain) *dtov1.ShopDomain {
	if out == nil {
		out = &dtov1.ShopDomain{}
	}

	out.Id = uint64(result.ID)
	out.DomainName = result.DomainName
	out.Type = dtov1.ShopDomainType(result.Type)
	out.IsEnabled = common.YesNo(result.IsEnabled)

	return out
}
