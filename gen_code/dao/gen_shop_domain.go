package dao

import (
	"reflect"
	"time"

	common "gitlab.com/howmay/gopher/common/v1"
	"gorm.io/gorm"
)

type ShopDomainWhereOption struct {
	ShopDomain
	Pagination *common.Pagination
	BaseWhere  *common.BaseWhere
	Sorting    *common.Sorting
}

func NewShopDomainWhereOption() *ShopDomainWhereOption {
	return &ShopDomainWhereOption{}
}

func (where *ShopDomainWhereOption) Where(db *gorm.DB) *gorm.DB {
	if where.BaseWhere != nil {
		db = db.Scopes(where.BaseWhere.Where)
	}
	return db
}

func (where *ShopDomainWhereOption) ID(in uint64) *ShopDomainWhereOption {
	where.ShopDomain.ID = in
	return where
}

func (where *ShopDomainWhereOption) CreatedAt(in time.Time) *ShopDomainWhereOption {
	where.ShopDomain.CreatedAt = in
	return where
}

func (where *ShopDomainWhereOption) UpdatedAt(in time.Time) *ShopDomainWhereOption {
	where.ShopDomain.UpdatedAt = in
	return where
}

func (where *ShopDomainWhereOption) DeletedAt(in gorm.DeletedAt) *ShopDomainWhereOption {
	where.ShopDomain.DeletedAt = in
	return where
}

func (where *ShopDomainWhereOption) ShopID(in uint64) *ShopDomainWhereOption {
	where.ShopDomain.ShopID = in
	return where
}

func (where *ShopDomainWhereOption) DomainName(in string) *ShopDomainWhereOption {
	where.ShopDomain.DomainName = in
	return where
}

func (where *ShopDomainWhereOption) Type(in ShopDomainType) *ShopDomainWhereOption {
	where.ShopDomain.Type = in
	return where
}

func (where *ShopDomainWhereOption) IsEnabled(in common.YesNo) *ShopDomainWhereOption {
	where.ShopDomain.IsEnabled = in
	return where
}

func (where *ShopDomainWhereOption) Page(db *gorm.DB) *gorm.DB {
	if where.Pagination != nil {
		return where.Pagination.LimitAndOffset(db)
	}
	return db
}

func (where *ShopDomainWhereOption) Sort(db *gorm.DB) *gorm.DB {
	if where.Sorting != nil {
		where.Sorting.Sort(db)
	}
	return db
}

func (where *ShopDomainWhereOption) IsEmptyWhereOpt() bool {
	return reflect.DeepEqual(where.ShopDomain, ShopDomain{})
}

func (where *ShopDomainWhereOption) TableName() string {
	return where.ShopDomain.TableName()
}

func (where *ShopDomainWhereOption) Preload(db *gorm.DB) *gorm.DB {
	return db
}

func (where *ShopDomainWhereOption) WithoutCount() bool {
	return where.Pagination.WithCount
}

type ShopDomainUpdateColumn struct {
	Values map[string]interface{}
}

func NewShopDomainUpdateColumn() *ShopDomainUpdateColumn {
	return &ShopDomainUpdateColumn{
		Values: make(map[string]interface{}),
	}
}

func (col *ShopDomainUpdateColumn) Columns() interface{} {
	return col
}

func (col *ShopDomainUpdateColumn) ID(in uint64) {
	col.Values["id"] = in
}

func (col *ShopDomainUpdateColumn) CreatedAt(in time.Time) {
	col.Values["created_at"] = in
}

func (col *ShopDomainUpdateColumn) UpdatedAt(in time.Time) {
	col.Values["updated_at"] = in
}

func (col *ShopDomainUpdateColumn) DeletedAt(in gorm.DeletedAt) {
	col.Values["deleted_at"] = in
}

func (col *ShopDomainUpdateColumn) ShopID(in uint64) {
	col.Values["shop_id"] = in
}

func (col *ShopDomainUpdateColumn) DomainName(in string) {
	col.Values["domain_name"] = in
}

func (col *ShopDomainUpdateColumn) Type(in ShopDomainType) {
	col.Values["type"] = in
}

func (col *ShopDomainUpdateColumn) IsEnabled(in common.YesNo) {
	col.Values["is_enabled"] = in
}
