package main

import (
	"fmt"
	"os"

	"github.com/spf13/cobra"
	"gitlab.com/howmay/gopher/gen_code"
)

var rootCmd = &cobra.Command{Use: "gen"}

func main() {
	rootCmd.AddCommand(gen_code.Cmd)

	if err := rootCmd.Execute(); err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
}
