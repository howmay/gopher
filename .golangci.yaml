run:
  # Timeout for analysis, e.g. 30s, 5m.
  # Default: 1m
  timeout: 3m
  skip-dirs:
    - "vendor"
    - "test"
  tests: false
  skip-files:
    - ".*_mock.go$"

# This file contains only configs which differ from defaults.
# All possible options can be found here https://github.com/golangci/golangci-lint/blob/master/.golangci.reference.yml
linters-settings:
  gosec:
    confidence: "low"
    exclude-use-default: false
    excludes:
      - "G101" # 查找硬编码凭证
      - "G304" # 通过污点输入提供的文件路径
      - "G307" # 把返回错误的函数放到 defer 内
      - "G401" # Detect the usage of DES, RC4, MD5 or SHA1
      - "G402" # Look for bad TLS connection settings
      - "G404" # Insecure random number source (rand)
      - "G501" # Import blocklist: crypto/md5
      - "G505" # Import blocklist: crypto/sha1
    severity: "low"

  errcheck:
    # Report about not checking of errors in type assertions: `a := b.(MyStruct)`.
    # Such cases aren't reported by default.
    # Default: false
    check-type-assertions: true

  exhaustive:
    # Program elements to check for exhaustiveness.
    # Default: [ switch ]
    check:
      - switch
      - map

  gocritic:
    # Settings passed to gocritic.
    # The settings key is the name of a supported gocritic checker.
    # The list of supported checkers can be find in https://go-critic.github.io/overview.
    settings:
      captLocal:
        # Whether to restrict checker to params only.
        # Default: true
        paramsOnly: false
      underef:
        # Whether to skip (*x).method() calls where x is a pointer receiver.
        # Default: true
        skipRecvDeref: false
    disabled-checks:
      - "ifElseChain" # 使用 if, else if, else 會被建議使用 switch

  govet:
    # Enable all analyzers.
    # Default: false
    enable-all: true
    # Disable analyzers by name.
    # Run `go tool vet help` to see all analyzers.
    # Default: []
    disable:
      - fieldalignment # too strict
      # - shadow
    # Settings per analyzer.
    settings:
      shadow:
        # Whether to be strict about shadowing; can be noisy.
        # Default: false
        strict: false

  nakedret:
    # Make an issue if func has more lines of code than this setting, and it has naked returns.
    # Default: 30
    max-func-lines: 0

  nolintlint:
    # Exclude following linters from requiring an explanation.
    # Default: []
    allow-no-explanation: [funlen, gocognit, lll]
    # Enable to require an explanation of nonzero length after each nolint directive.
    # Default: false
    require-explanation: true
    # Enable to require nolint directives to mention the specific linter being suppressed.
    # Default: false
    require-specific: true

  rowserrcheck:
    # database/sql is always checked
    # Default: []
    packages:
      - github.com/jmoiron/sqlx

  tenv:
    # The option `all` will run against whole test files (`_test.go`) regardless of method/function signatures.
    # Otherwise, only methods that take `*testing.T`, `*testing.B`, and `testing.TB` as arguments are checked.
    # Default: false
    all: true

  revive:
    severity: warning
    rules:
      - name: time-naming
        severity: warning
        disabled: true

linters:
  disable-all: true
  enable:
    ## enabled by default
    # - errcheck # checking for unchecked errors, these unchecked errors can be critical bugs in some cases, 有太多 .() 轉換 type 沒確認 ok
    - gosimple # specializes in simplifying a code
    - govet # reports suspicious constructs, such as Printf calls whose arguments do not align with the format string
    - ineffassign # detects when assignments to existing variables are not used
    - staticcheck # is a go vet on steroids, applying a ton of static analysis checks
    - typecheck # like the front-end of a Go compiler, parses and type-checks Go code
    - unused # checks for unused constants, variables, functions and types
    ## disabled by default
    - asasalint # checks for pass []any as any in variadic func(...any)
    - asciicheck # checks that your code does not contain non-ASCII identifiers
    - bidichk # checks for dangerous unicode character sequences
    - bodyclose # checks whether HTTP response body is closed successfully
    # - cyclop # checks function and package cyclomatic complexity 檢測代碼複雜度 https://juejin.cn/post/7033407841226948622
    # - dupl # tool for code clone detection
    - durationcheck # checks for two durations multiplied together
    - errname # checks that sentinel errors are prefixed with the Err and error types are suffixed with the Error
    - errorlint # finds code that will cause problems with the error wrapping scheme introduced in Go 1.13
    - execinquery # checks query string in Query function which reads your Go src files and warning it finds
    # - exhaustive # checks exhaustiveness of enum switch statements, switch 參數缺少了其他相同 type 參數
    - exportloopref # checks for pointers to enclosing loop variables
    - forbidigo # forbids identifiers
    # - funlen # tool for detection of long functions 行數太多
    - gocheckcompilerdirectives # validates go compiler directive comments (//go:)
    # - gochecknoglobals # checks that no global variables exist
    - gochecknoinits # checks that no init functions are present in Go code
    # - gocognit # computes and checks the cognitive complexity of functions 複雜度太高
    - goconst # finds repeated strings that could be replaced by a constant
    - gocritic # provides diagnostics that check for bugs, performance and style issues
    # - gocyclo # computes and checks the cyclomatic complexity of functions 複雜度太高
    # - godot # checks if comments end in a period
    - goimports # in addition to fixing imports, goimports also formats your code in the same style as gofmt
    # - gomnd # detects magic numbers
    # - gomoddirectives # manages the use of 'replace', 'retract', and 'excludes' directives in go.mod
    # - gomodguard # allow and block lists linter for direct Go module dependencies. This is different from depguard where there are different block types for example version constraints and module recommendations
    - goprintffuncname # checks that printf-like functions are named with f at the end
    - gosec # inspects source code for security problems
    # - lll # reports long lines
    - loggercheck # checks key value pairs for common logger libraries (kitlog,klog,logr,zap)
    - makezero # finds slice declarations with non-zero initial length
    - mirror # reports wrong mirror patterns of bytes/strings usage
    # - musttag # enforces field tags in (un)marshaled structs
    # - nakedret # finds naked returns in functions greater than a specified function length, naked return
    # - nestif # reports deeply nested if statements, 複雜度檢測 if 包含了多少行數
    # - nilnil # checks that there is no simultaneous return of nil error and an invalid value, 回傳 nil,nil
    # - noctx # finds sending http request without context.Context
    # - nolintlint # reports ill-formed or insufficient nolint directives
    # - nonamedreturns # reports all named returns
    - nosprintfhostport # checks for misuse of Sprintf to construct a host with port in a URL
    - predeclared # finds code that shadows one of Go's predeclared identifiers
    - promlinter # checks Prometheus metrics naming via promlint
    - reassign # checks that package variables are not reassigned
    - revive # fast, configurable, extensible, flexible, and beautiful linter for Go, drop-in replacement of golint
    - rowserrcheck # checks whether Err of rows is checked successfully
    # - sqlclosecheck # checks that sql.Rows and sql.Stmt are closed
    - stylecheck # is a replacement for golint
    - tenv # detects using os.Setenv instead of t.Setenv since Go1.17
    - testableexamples # checks if examples are testable (have an expected output)
    - testpackage # makes you use a separate _test package
    - tparallel # detects inappropriate usage of t.Parallel() method in your Go test codes
    - unconvert # removes unnecessary type conversions
    # - unparam # reports unused function parameters, 回傳值沒使用到
    - usestdlibvars # detects the possibility to use variables/constants from the Go standard library
    # - wastedassign # finds wasted assignment statements, 不需要賦值
    # - whitespace # detects leading and trailing whitespace, 空白檢查

    ## you may want to enable
    - decorder # checks declaration order and count of types, constants, variables and functions
    # - exhaustruct # [highly recommend to enable] checks if all structure fields are initialized
    # - gci # controls golang package import order and makes it always deterministic
    - ginkgolinter # [if you use ginkgo/gomega] enforces standards of using ginkgo and gomega
    # - godox # detects FIXME, TODO and other comment keywords
    - goheader # checks is file header matches to pattern
    # - interfacebloat # checks the number of methods inside an interface
    # - ireturn # accept interfaces, return concrete types
    - prealloc # [premature optimization, but can be used in some cases] finds slice declarations that could potentially be preallocated
    # - tagalign # checks that struct tags are well aligned
    # - varnamelen # [great idea, but too many false positives] checks that the length of a variable's name matches its scope, 變數名稱太短
    # - wrapcheck # checks that errors returned from external packages are wrapped, 回傳的 error 需要 wrap
    #- zerologlint # detects the wrong usage of zerolog that a user forgets to dispatch zerolog.Event

    ## disabled
    #- containedctx # detects struct contained context.Context field
    #- contextcheck # [too many false positives] checks the function whether use a non-inherited context
    #- depguard # [replaced by gomodguard] checks if package imports are in a list of acceptable packages
    #- dogsled # checks assignments with too many blank identifiers (e.g. x, _, _, _, := f())
    #- dupword # [useless without config] checks for duplicate words in the source code
    #- errchkjson # [don't see profit + I'm against of omitting errors like in the first example https://github.com/breml/errchkjson] checks types passed to the json encoding functions. Reports unsupported types and optionally reports occasions, where the check for the returned error can be omitted
    #- forcetypeassert # [replaced by errcheck] finds forced type assertions
    #- goerr113 # [too strict] checks the errors handling expressions
    #- gofmt # [replaced by goimports] checks whether code was gofmt-ed
    #- gofumpt # [replaced by goimports, gofumports is not available yet] checks whether code was gofumpt-ed
    #- gosmopolitan # reports certain i18n/l10n anti-patterns in your Go codebase
    #- grouper # analyzes expression groups
    #- importas # enforces consistent import aliases
    #- maintidx # measures the maintainability index of each function
    #- misspell # [useless] finds commonly misspelled English words in comments
    #- nlreturn # [too strict and mostly code is not more readable] checks for a new line before return and branch statements to increase code clarity
    #- paralleltest # [too many false positives] detects missing usage of t.Parallel() method in your Go test
    #- tagliatelle # checks the struct tags
    #- thelper # detects golang test helpers without t.Helper() call and checks the consistency of test helpers
    #- wsl # [too strict and mostly code is not more readable] whitespace linter forces you to use empty lines

issues:
  # Maximum count of issues with the same text.
  # Set to 0 to disable.
  # Default: 3
  max-same-issues: 50
  exclude:
    - "at least one file in a package should have a package comment"
    - "(comment on exported (method|function|type|const)|should have( a package)? comment|comment should be of the form)"
    - 'declaration of "(err|ctx|ok|logger)" shadows declaration at'
    - 'Error return value of .((os\.)?std(out|err)\..*|.*Close|.*Flush|os\.Remove(All)?|.*print(f|ln)?|os\.(Un)?Setenv). is not checked'
  exclude-rules:
    - source: "(noinspection|TODO)"
      linters: [godot]
    - source: "//noinspection"
      linters: [gocritic]
    - path: "_test\\.go"
      linters:
        - bodyclose
        - dupl
        - funlen
        - goconst
        - gosec
        - noctx
        - wrapcheck
