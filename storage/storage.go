package storage

import (
	"bytes"
	"context"
	"io"
	"strings"
	"time"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/credentials"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/s3"
	"gitlab.com/howmay/gopher/errors"
)

const (
	// restful file url path
	AppFileRestfulURI = "/app/apis/v1/files"
	// restful file url path
	PlatformFileRestfulURI = "/b/apis/v1/files"
)

type Config struct {
	AccessKey string `mapstructure:"access_key"`
	SecretKey string `mapstructure:"secret_key"`
	Region    string `mapstructure:"region"`
	Bucket    string `mapstructure:"bucket"`
}

type S3 struct {
	session *s3.S3
	bucket  *string
}

type StorageS3 interface {
	ListFiles(ctx context.Context) ([]*s3.Object, error)
	UploadFile(ctx context.Context, key, body string) error
	UploadFileByReader(ctx context.Context, key string, body io.Reader, expire *time.Time) error
	GetFile(ctx context.Context, key string) (string, error)
	DeleteFile(ctx context.Context, key string) error
}

func New(cfg *Config) (StorageS3, error) {
	s3Session := s3.New(session.Must(session.NewSession(&aws.Config{
		Region:      aws.String(cfg.Region),
		Credentials: credentials.NewStaticCredentials(cfg.AccessKey, cfg.SecretKey, ""),
	})))

	return &S3{
		session: s3Session,
		bucket:  aws.String(cfg.Bucket),
	}, nil
}

func (s *S3) ListFiles(ctx context.Context) ([]*s3.Object, error) {
	output, err := s.session.ListObjectsV2WithContext(ctx, &s3.ListObjectsV2Input{
		Bucket: s.bucket,
	})
	if err != nil {
		return nil, errors.ConvertAWSError(err)
	}

	return output.Contents, nil
}

func (s *S3) UploadFile(ctx context.Context, key, body string) error {
	_, err := s.session.PutObjectWithContext(ctx, &s3.PutObjectInput{
		Bucket: s.bucket,
		Key:    aws.String(key),
		Body:   strings.NewReader(body),
		ACL:    aws.String(s3.BucketCannedACLPrivate),
	})

	if err != nil {
		return errors.ConvertAWSError(err)
	}
	return nil
}

func (s *S3) UploadFileByReader(ctx context.Context, key string, body io.Reader, expire *time.Time) error {
	tmp, err := io.ReadAll(body)
	if err != nil {
		return errors.Wrapf(errors.ErrInternalError, "fail to read to byte")
	}
	r := bytes.NewReader(tmp)
	_, err = s.session.PutObjectWithContext(ctx, &s3.PutObjectInput{
		Bucket:  s.bucket,
		Key:     aws.String(key),
		Body:    r,
		ACL:     aws.String(s3.BucketCannedACLPrivate),
		Expires: expire,
	})

	if err != nil {
		return errors.ConvertAWSError(err)
	}
	return nil
}

func (s *S3) GetFile(ctx context.Context, key string) (string, error) {
	out, err := s.session.GetObjectWithContext(ctx, &s3.GetObjectInput{
		Bucket: s.bucket,
		Key:    &key,
	})
	if err != nil {
		return "", errors.ConvertAWSError(err)
	}

	body, err := io.ReadAll(out.Body)
	if err != nil {
		return "", errors.Wrapf(errors.ErrInternalError, "fail to read body, err: %s", err.Error())
	}
	return string(body), nil
}

func (s *S3) DeleteFile(ctx context.Context, key string) error {
	_, err := s.session.DeleteObjectsWithContext(ctx, &s3.DeleteObjectsInput{
		Bucket: s.bucket,
		Delete: &s3.Delete{
			Objects: []*s3.ObjectIdentifier{
				{
					Key: &key,
				},
			},
		},
	})
	if err != nil {
		return errors.ConvertAWSError(err)
	}
	return nil
}
