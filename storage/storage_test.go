package storage

import (
	"context"
	"fmt"
	"testing"

	"github.com/aws/aws-sdk-go/service/s3"
)

func TestNew(t *testing.T) {
	type args struct {
		cfg *Config
	}
	tests := []struct {
		name    string
		args    args
		want    *s3.S3
		wantErr bool
	}{
		{
			name: "normal test",
			args: args{
				cfg: &Config{
					AccessKey: "AKIASVCMCTQOJDLKSFZU",
					SecretKey: "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",
					Region:    "ap-east-1",
					Bucket:    "pre-bochat",
				},
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			_, err := New(tt.args.cfg)
			if (err != nil) != tt.wantErr {
				t.Errorf("New() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
		})
	}
}

func TestS3_ListFiles(t *testing.T) {
	s, _ := New(&Config{
		AccessKey: "AKIASVCMCTQOJDLKSFZU",
		SecretKey: "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",
		Region:    "ap-east-1",
		Bucket:    "pre-bochat",
	})
	tests := []struct {
		name    string
		wantErr bool
	}{
		{
			name: "normal test",
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			objs, err := s.ListFiles(context.Background())
			if (err != nil) != tt.wantErr {
				t.Errorf("S3.ListFiles() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			fmt.Printf("%+v", objs)
		})
	}
}

func TestS3_UploadFile(t *testing.T) {
	s, _ := New(&Config{
		AccessKey: "AKIASVCMCTQOJDLKSFZU",
		SecretKey: "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",
		Region:    "ap-east-1",
		Bucket:    "pre-bochat",
	})

	type args struct {
		ctx  context.Context
		key  string
		body string
	}
	tests := []struct {
		name    string
		args    args
		wantErr bool
	}{
		{
			name: "normal test",
			args: args{
				ctx:  context.Background(),
				key:  "asdasdsdasdad.png",
				body: "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAJYAAACWBAMAAADOL2zRAAAAG1BMVEXMzMyWlpbFxcW+vr6cnJyxsbG3t7eqqqqjo6M0o99qAAAACXBIWXMAAA7EAAAOxAGVKw4bAAAA4klEQVRoge3RMU+DQBjG8ecFah0PB1mBNMYRGz8AsTauNNJ0bac6qoNxtIbwueUadAdG/7/lnrvhyd17EgAAAAAAAAD8eyY5v9h516pWlvbnNq1rp1N6/3s+setFT8VuUlf4WHVd9VrKwrb68On6FDSHwV1x7BbLB7PZ922l6Ob5a+VTcjd/fxvclefuqK1ZVISlLo/70vl0ZVGaDe7q3riJE7Pz4GZ12pY+uW4/al5b5f29wkafrz65sfdaLFf9vIJEh71PzubVZkzXxd8/aq2s8MlZ0AzuAgAAAAAAAIBJfgDlxh4Tsy3PVwAAAABJRU5ErkJggg==",
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if err := s.UploadFile(tt.args.ctx, tt.args.key, tt.args.body); (err != nil) != tt.wantErr {
				t.Errorf("S3.UploadFile() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}

func TestS3_GetFile(t *testing.T) {
	s, _ := New(&Config{
		AccessKey: "AKIASVCMCTQOJDLKSFZU",
		SecretKey: "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",
		Region:    "ap-east-1",
		Bucket:    "pre-bochat",
	})

	type args struct {
		ctx context.Context
		key string
	}
	tests := []struct {
		name    string
		args    args
		want    string
		wantErr bool
	}{
		{
			name: "normal test",
			args: args{
				ctx: context.Background(),
				key: "asdasdsdasdad.png",
			},
			want: "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAJYAAACWBAMAAADOL2zRAAAAG1BMVEXMzMyWlpbFxcW+vr6cnJyxsbG3t7eqqqqjo6M0o99qAAAACXBIWXMAAA7EAAAOxAGVKw4bAAAA4klEQVRoge3RMU+DQBjG8ecFah0PB1mBNMYRGz8AsTauNNJ0bac6qoNxtIbwueUadAdG/7/lnrvhyd17EgAAAAAAAAD8eyY5v9h516pWlvbnNq1rp1N6/3s+setFT8VuUlf4WHVd9VrKwrb68On6FDSHwV1x7BbLB7PZ922l6Ob5a+VTcjd/fxvclefuqK1ZVISlLo/70vl0ZVGaDe7q3riJE7Pz4GZ12pY+uW4/al5b5f29wkafrz65sfdaLFf9vIJEh71PzubVZkzXxd8/aq2s8MlZ0AzuAgAAAAAAAIBJfgDlxh4Tsy3PVwAAAABJRU5ErkJggg==",
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := s.GetFile(tt.args.ctx, tt.args.key)
			if (err != nil) != tt.wantErr {
				t.Errorf("S3.GetFile() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if got != tt.want {
				t.Errorf("S3.GetFile() = %v, want %v", got, tt.want)
			}
		})
	}
}
