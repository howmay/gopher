gen:
	make gen.proto.common

gen.proto.common:
	protoc -I$(CURDIR) -I vendor/ --go_out=paths=source_relative,\
	Mgoogle/protobuf/timestamp.proto=github.com/gogo/protobuf/types,\
	Mgoogle/protobuf/wrappers.proto=github.com/gogo/protobuf/types,\
	Mgoogle/protobuf/empty.proto=github.com/gogo/protobuf/types:. $(CURDIR)/common/v1/*.proto

analyse:
	golangci-lint run --fix --timeout 3m --build-tags=dynamic