package errors

import (
	"context"
	"encoding/json"
	"fmt"
	"net/http"
	"strconv"
	"strings"

	"github.com/pkg/errors"
	"google.golang.org/grpc"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/metadata"
	"google.golang.org/grpc/status"
)

// 自定義的 errors
var (
	ErrBadRequest      = &_error{Code: 400000, Message: http.StatusText(http.StatusBadRequest), Status: http.StatusBadRequest}
	ErrInvalidInput    = &_error{Code: 400001, Message: "One of the request inputs is not valid.", Status: http.StatusBadRequest, GRPCCode: codes.InvalidArgument}
	ErrResultBeenBound = &_error{Code: 400002, Message: "The result already been bound, Need to check which resource has been used", Status: http.StatusBadRequest, GRPCCode: codes.InvalidArgument}

	ErrUnauthorized                  = &_error{Code: 401001, Message: http.StatusText(http.StatusUnauthorized), Status: http.StatusUnauthorized, GRPCCode: codes.Unauthenticated}
	ErrTokenUnavailable              = &_error{Code: 401002, Message: "token not found", Status: http.StatusUnauthorized, GRPCCode: codes.Unauthenticated}
	ErrUsernameOrPasswordUnavailable = &_error{Code: 401003, Message: "username or password is unavailable", Status: http.StatusUnauthorized, GRPCCode: codes.Unauthenticated}
	ErrInvalidAuthenticationInfo     = &_error{Code: 401004, Message: "The authentication information was not provided in the correct format. Verify the value of Authorization header.", Status: http.StatusUnauthorized, GRPCCode: codes.Unauthenticated}

	ErrForbidden  = &_error{Code: 403000, Message: http.StatusText(http.StatusForbidden), Status: http.StatusForbidden}
	ErrNotAllowed = &_error{Code: 403001, Message: "The request is understood, but it has been refused or access is not allowed.", Status: http.StatusForbidden, GRPCCode: codes.PermissionDenied}

	ErrNotFound         = &_error{Code: 404000, Message: http.StatusText(http.StatusNotFound), Status: http.StatusNotFound}
	ErrResourceNotFound = &_error{Code: 404001, Message: "The specified resource does not exist.", Status: http.StatusNotFound, GRPCCode: codes.NotFound}

	ErrMethodNotAllowed = &_error{Code: 405001, Message: "Server has received and recognized the request, but has rejected the specific HTTP method it’s using.", Status: http.StatusMethodNotAllowed, GRPCCode: codes.Unavailable}

	ErrRequestTime = &_error{Code: 408001, Message: "request time out", Status: http.StatusRequestTimeout, GRPCCode: codes.DeadlineExceeded}

	ErrConflict                 = &_error{Code: 409000, Message: http.StatusText(http.StatusConflict), Status: http.StatusConflict, GRPCCode: codes.AlreadyExists}
	ErrResourceAlreadyExists    = &_error{Code: 409001, Message: "The specified resource already exists.", Status: http.StatusConflict, GRPCCode: codes.AlreadyExists}
	ErrPostgresLockNotAvailable = &_error{Code: 409002, Message: "lock not available", Status: http.StatusConflict, GRPCCode: codes.AlreadyExists}

	ErrInternalServerError = &_error{Code: 500000, Message: http.StatusText(http.StatusInternalServerError), Status: http.StatusInternalServerError, GRPCCode: codes.Internal}
	ErrInternalError       = &_error{Code: 500001, Message: "The server encountered an internal error. Please retry the request.", Status: http.StatusInternalServerError, GRPCCode: codes.Internal}
)

type _error struct {
	Status   int                    `json:"status"`
	Code     int                    `json:"code"`
	GRPCCode codes.Code             `json:"grpc_code"`
	Message  string                 `json:"message"`
	Details  map[string]interface{} `json:"details"`
}

// HTTPError ...
type HTTPError struct {
	Status  int                    `json:"-"`
	Code    int                    `json:"code"`
	Message string                 `json:"message"`
	Details map[string]interface{} `json:"details,omitempty"`
}

func (e *_error) Error() string {
	var b strings.Builder
	_, _ = b.WriteRune('[')
	_, _ = b.WriteString(strconv.Itoa(e.Code))
	_, _ = b.WriteRune(']')
	_, _ = b.WriteRune(' ')
	_, _ = b.WriteString(e.Message)
	return b.String()
}

// Is ...
func (e *_error) Is(target error) bool {
	var tErr *_error
	if !errors.As(target, &tErr) {
		return false
	}
	return e.Code == tErr.Code
}

// GetHTTPError ,,,
func GetHTTPError(err *_error) HTTPError {
	return HTTPError{
		Status:  err.Status,
		Message: err.Message,
		Code:    err.Code,
		Details: err.Details,
	}
}

// ConvertToHTTPError 嘗試從 _error 轉換成 HTTPError
func ConvertToHTTPError(err error) HTTPError {
	var _err *_error
	if !errors.As(errors.Cause(err), &_err) {
		_err = &_error{
			Code:    ErrInternalError.Code,
			Message: ErrInternalError.Message,
		}

		s := status.Convert(err)
		if s == nil {
			return GetHTTPError(_err)
		}
		switch s.Code() {
		case InvalidArgument:
			_err = ErrInvalidInput
		case NotFound:
			_err = ErrResourceNotFound
		case AlreadyExists:
			_err = ErrResourceAlreadyExists
		case PermissionDenied:
			_err = ErrNotAllowed
		case Unauthenticated:
			_err = ErrUnauthorized
		case OutOfRange:
			_err = ErrInvalidInput
		case Unimplemented:
			_err = ErrMethodNotAllowed
		default:
			_err = ErrInternalError
		}
		_err.Message = err.Error()

		return GetHTTPError(_err)
	}
	return GetHTTPError(_err)
}

// NewWithMessage 抽換錯誤訊息
// 未定義的錯誤會被視為 ErrInternalError 類型
func NewWithMessage(err error, message string, args ...interface{}) error {
	if err == nil {
		return nil
	}
	var _err *_error
	if !errors.As(errors.Cause(err), &_err) {
		return WithStack(&_error{
			Status:   ErrInternalError.Status,
			Code:     ErrInternalError.Code,
			Message:  ErrInternalError.Message,
			GRPCCode: ErrInternalError.GRPCCode,
		})
	}

	err = &_error{
		Status:   _err.Status,
		Code:     _err.Code,
		Message:  message,
		GRPCCode: _err.GRPCCode,
	}
	var msg string
	for i := 0; i < len(args); i++ {
		msg += "%+v"
	}
	return Wrapf(err, msg, args...)
}

// WithErrors 使用訂好的errors code 與訊息,如果未定義message 顯示對應的http status描述
func WithErrors(err error) error {
	if err == nil {
		return nil
	}
	var _err *_error
	if !errors.As(errors.Cause(err), &_err) {
		return WithStack(&_error{
			Status:  ErrInternalError.Status,
			Code:    ErrInternalError.Code,
			Message: http.StatusText(ErrInternalError.Status),
		})
	}
	return WithStack(&_error{
		Status:  _err.Status,
		Code:    _err.Code,
		Message: _err.Message,
	})
}

// SetDetails set details as you wish =)
func (e *_error) SetDetails(details map[string]interface{}) {
	e.Details = details
}

// CompareErrorCode 比較兩個錯誤代碼是否一致
func CompareErrorCode(errA error, errB error) bool {
	var aErr, bErr *_error
	if !errors.As(errors.Cause(errA), &aErr) {
		return false
	}
	if !errors.As(errors.Cause(errB), &bErr) {
		return false
	}
	if aErr.Code == bErr.Code {
		return true
	}
	return false
}

// ConvertProtoErr Convert _error to grpc error
func ConvertProtoErr(ctx context.Context, err error) error {
	if err == nil {
		return nil
	}
	var _err *_error
	if !errors.As(errors.Cause(err), &_err) {
		return status.Error(ErrInternalError.GRPCCode, err.Error())
	}
	b, _ := json.Marshal(_err)
	st := status.New(_err.GRPCCode, string(b))
	trailers := metadata.Pairs("error", string(b))
	grpc.SetTrailer(ctx, trailers)

	return st.Err()
}

// ConvertHttpErr Convert  grpc error to _error
func ConvertHTTPErr(err error) error {
	if err == nil {
		return nil
	}
	s := status.Convert(err)
	if s == nil {
		return ErrInternalError
	}
	interErr := _error{}
	jerr := json.Unmarshal([]byte(s.Message()), &interErr)
	if jerr != nil {
		return switchCode(s)
	}
	return WithStack(&interErr)
}

func switchCode(s *status.Status) error {
	httperr := ErrInternalError
	switch s.Code() {
	case Unknown:
		httperr = ErrInternalError
	case InvalidArgument:
		httperr = ErrInvalidInput
	case NotFound:
		httperr = ErrResourceNotFound
	case AlreadyExists:
		httperr = ErrResourceAlreadyExists
	case PermissionDenied:
		httperr = ErrNotAllowed
	case Unauthenticated:
		httperr = ErrUnauthorized
	case OutOfRange:
		httperr = ErrInvalidInput
	case Internal:
		httperr = ErrInternalError
	case DataLoss:
		httperr = ErrInternalError
	}
	httperr.Message = s.Message()
	return WithStack(httperr)
}

// NewWithMessagef 抽換錯誤訊息
func NewWithMessagef(err error, format string, args ...interface{}) error {
	return NewWithMessage(err, fmt.Sprintf(format, args...))
}

// GetCodeWithErrors 使用訂好的errors code 與訊息,如果未定義message 顯示對應的http status描述
func GetCodeWithErrors(err error) (int, string) {
	var _err *_error
	if !errors.As(errors.Cause(err), &_err) {
		return ErrInternalError.Code, ErrInternalError.Message
	}
	return _err.Code, _err.Message
}

// HTTPConvertToError 將 http 的 response body convert to _error
func HTTPConvertToError(b []byte) error {
	interErr := _error{}
	jErr := json.Unmarshal(b, &interErr)
	if jErr != nil {
		return ErrInternalError
	}
	return WithStack(&interErr)
}

// GetStatusWithErrors 取得 error 的 http status code
func GetStatusWithErrors(err error) int {
	var _err *_error
	if !errors.As(errors.Cause(err), &_err) {
		return http.StatusInternalServerError
	}
	return _err.Status
}

// GetProtoCodeWithErrors 使用訂好的errors grpc code
func GetGrpcCodeWithErrors(err error) codes.Code {
	var _err *_error
	if !errors.As(errors.Cause(err), &_err) {
		return ErrInternalError.GRPCCode
	}
	return _err.GRPCCode
}
