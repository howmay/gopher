package errors

import (
	"context"
	"encoding/json"
	"net/http"

	"github.com/labstack/echo/v4"
	"github.com/pkg/errors"
	"github.com/rs/zerolog/log"
	"google.golang.org/grpc"
	"google.golang.org/grpc/status"
)

// UnaryClientErrorInterceptor ...
func UnaryClientErrorInterceptor() grpc.UnaryClientInterceptor {
	return func(parentCtx context.Context, method string, req, reply interface{}, cc *grpc.ClientConn, invoker grpc.UnaryInvoker, opts ...grpc.CallOption) error {
		lastErr := invoker(ContextWithMeta(parentCtx, XRequestIDFromContext(parentCtx)), method, req, reply, cc, opts...)
		return ConvertHTTPErr(lastErr)
	}
}

// UnaryErrorInterceptor ...
func UnaryErrorInterceptor() grpc.UnaryServerInterceptor {
	return func(ctx context.Context, req interface{}, info *grpc.UnaryServerInfo, handler grpc.UnaryHandler) (_ interface{}, err error) {

		resp, err := handler(ctx, req)
		if err != nil {
			logFields := map[string]interface{}{}
			requestID := MetaFromContext(ctx)
			logFields["request_id"] = requestID
			logFields["input"] = req
			logFields["output"] = resp

			var _err *_error
			if !errors.As(errors.Cause(err), &_err) {
				return resp, status.Error(ErrInternalError.GRPCCode, err.Error())
			}
			// 根據狀態碼用不同等級來紀錄
			logger := log.With().Fields(logFields).Logger()
			if _err.Status >= http.StatusInternalServerError {
				logger.Error().Msgf("%+v", err)
			} else {
				logger.Debug().Msgf("%+v", err)
			}
			return resp, ConvertProtoErr(ctx, err)
		}
		return resp, err
	}
}

// HTTPErrorHandlerForEcho responds error response according to given error.
func HTTPErrorHandlerForEcho(err error, c echo.Context) {
	if err == nil {
		return
	}

	var echoErr *echo.HTTPError
	if errors.As(err, &echoErr) {
		_ = c.JSON(echoErr.Code, echoErr)
		return
	}

	var _err *_error
	if !errors.As(errors.Cause(err), &_err) {
		_ = c.JSON(http.StatusInternalServerError, ErrInternalError)
		return
	}

	if _err.Code/10000 == 3 {
		_ = c.JSON(http.StatusInternalServerError, ErrInternalError)
		return
	}
	_ = c.JSON(_err.Status, GetHTTPError(_err))
}

// ErrMiddleware provide error middleware
func ErrMiddleware(next echo.HandlerFunc) echo.HandlerFunc {
	return func(c echo.Context) error {
		err := next(c)
		if err != nil {
			logFields := map[string]interface{}{}

			// 紀錄 Request 資料
			req := c.Request()
			{
				logFields["req_method"] = req.Method
				logFields["req_url"] = req.URL.String()
			}

			// 紀錄 Response 資料
			resp := c.Response()
			resp.After(func() {
				logFields["response_status"] = resp.Status
				// 根據狀態碼用不同等級來紀錄
				logger := log.Ctx(req.Context()).With().Fields(logFields).Logger()
				if resp.Status >= http.StatusInternalServerError {
					logger.Error().Msgf("%+v", err)
				} else if resp.Status >= http.StatusBadRequest {
					logger.Warn().Msgf("%+v", err)
				}
			})
		}
		return err
	}

}

// NotFoundHandlerForEcho responds not found response.
func NotFoundHandlerForEcho(c echo.Context) error {
	return c.JSON(http.StatusNotFound, ErrNotFound)
}

// UnaryAccessInterceptor ...
func UnaryAccessInterceptor() grpc.UnaryServerInterceptor {
	return func(ctx context.Context, req interface{}, info *grpc.UnaryServerInfo, handler grpc.UnaryHandler) (_ interface{}, err error) {
		go func() {
			reqB, _ := json.Marshal(req)
			reqStr := string(reqB)
			requestID := MetaFromContext(ctx)
			logger := log.With().Str("request_id", requestID).Str("input", reqStr).Str("endpoint", info.FullMethod).Logger()
			logger.Debug().Msg("Access log")
		}()
		resp, err := handler(ctx, req)
		return resp, err
	}
}
