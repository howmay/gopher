package errors

import (
	"context"
	"strconv"
	"strings"

	"github.com/99designs/gqlgen/graphql"
	"github.com/aws/aws-sdk-go/aws/awserr"
	"github.com/aws/aws-sdk-go/aws/request"
	"github.com/go-sql-driver/mysql"
	"github.com/jackc/pgconn"
	"github.com/lib/pq"
	"github.com/olivere/elastic/v7"
	"github.com/pkg/errors"
	"github.com/rs/zerolog/log"
	"github.com/vektah/gqlparser/v2/gqlerror"
	"gitlab.com/howmay/gopher/ctxutil"
	"gorm.io/gorm"
)

func ConvertElasticSearchError(err error) error {
	if err == nil {
		return nil
	}

	if elastic.IsConnErr(err) {
		return errors.Wrapf(ErrInternalError, "Conn error: %s", err.Error())
	}
	if elastic.IsContextErr(err) {
		return errors.Wrapf(ErrInternalError, "Context error: %s", err.Error())
	}
	if elastic.IsNotFound(err) {
		return errors.Wrapf(ErrResourceNotFound, "Not found: %s", err.Error())
	}
	if elastic.IsTimeout(err) {
		return errors.Wrapf(ErrRequestTime, "request timeout: %s", err.Error())
	}

	return errors.Wrapf(ErrInternalError, "unknow error: %s", err.Error())
}

// GQLErrorPresenter ...
func GQLErrorPresenter(ctx context.Context, err error) *gqlerror.Error {
	var (
		logger    = log.Ctx(ctx).With().Logger()
		unwrapErr error
		path      = graphql.GetPath(ctx)
	)

	var gqlErr *gqlerror.Error
	if errors.As(errors.Cause(err), &gqlErr) {
		unwrapErr = gqlErr.Unwrap()
	} else {
		if path != nil {
			gqlErr = gqlerror.WrapPath(path, err)
		} else {
			gqlErr = gqlerror.Errorf(err.Error())
		}
	}

	if unwrapErr == nil {
		unwrapErr = err
	}

	if gqlErr.Extensions == nil {
		gqlErr.Extensions = make(map[string]interface{})
	}

	var _err *_error
	if !errors.As(Cause(unwrapErr), &_err) {
		gqlErr.Extensions["raw_err_msg"] = unwrapErr.Error()
		if strings.Contains(unwrapErr.Error(), "input") {
			_err = ErrInvalidInput
		} else {
			_err = ErrInternalError
		}
	}

	if _err.Details != nil {
		for k, v := range _err.Details {
			gqlErr.Extensions[k] = v
		}
	}

	gqlErr.Message = strconv.Itoa(_err.Code)
	gqlErr.Extensions["err_msg"] = _err.Message
	if Is(gqlErr, ErrInternalError) {
		gqlErr.Extensions["err_msg"] = ErrInternalServerError.Message
	}

	logger = logger.With().Fields(map[string]interface{}{
		"trace_id":   ctxutil.GetTraceIDFromContext(ctx),
		"access_log": true,
	}).Logger()

	switch {
	case _err.Status >= 400 && _err.Status < 500:
		logger.Warn().Msgf("error handle: %+v", unwrapErr)
	default:
		logger.Error().Msgf("error handle: %+v", unwrapErr)
	}

	return gqlErr
}

// ConvertMySQLError convert mysql error
func ConvertMySQLError(err error) error {
	if err == nil {
		return nil
	}

	if errors.Is(err, gorm.ErrRecordNotFound) {
		return errors.WithStack(ErrResourceNotFound)
	}

	var mysqlErr *mysql.MySQLError
	if errors.As(err, &mysqlErr) {
		if mysqlErr.Number == 1062 {
			// the duplicate key error.
			return errors.WithStack(ErrResourceAlreadyExists)
		}
	}

	return errors.Wrapf(ErrInternalError, err.Error())
}

func ConvertAWSError(err error) error {
	if err == nil {
		return nil
	}

	var aerr awserr.Error
	if !errors.As(err, &aerr) {
		return errors.Wrapf(ErrInternalError, "can't convert aws error, err: %s", err.Error())
	}

	switch aerr.Code() {
	case request.WaiterResourceNotReadyErrorCode:
		return errors.Wrapf(ErrBadRequest, "waiter resource not ready, err: %s", err.Error())
	default:
		return errors.Wrapf(ErrInternalError, "aws error, err: %s", err.Error())
	}
}

func ConvertRedisError(err error) error {
	if err == nil {
		return nil
	}
	return errors.Wrapf(ErrInternalError, "redis err, err: %s", err.Error())
}

// ConvertPostgresError convert postgres error
func ConvertPostgresError(err error) error {
	if err == nil {
		return nil
	}

	var pgErr *pq.Error
	if errors.As(err, &pgErr) {
		switch pgErr.Code {
		case "23505":
			return errors.WithStack(ErrResourceAlreadyExists)
		case "55P03":
			return errors.WithStack(ErrPostgresLockNotAvailable)
		}
	}

	var pqConnErr *pgconn.PgError
	if errors.As(err, &pqConnErr) {
		switch pgErr.Code {
		case "23505":
			return errors.WithStack(ErrResourceAlreadyExists)
		case "55P03":
			return errors.WithStack(ErrPostgresLockNotAvailable)
		}
	}

	if errors.Is(err, gorm.ErrRecordNotFound) {
		return errors.WithStack(ErrResourceNotFound)
	}

	return errors.WithStack(ErrInternalError)

}
