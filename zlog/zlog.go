// Package zlog 負責初始化 zerolog 的格式和等級
package zlog

import (
	"context"
	"fmt"
	"io"
	"os"
	"strconv"
	"time"

	"github.com/rs/zerolog"
	"github.com/rs/zerolog/log"
)

var (
	// Teal ...
	Teal = Color("\033[1;36m%s\033[0m")
	// Yellow ...
	Yellow = Color("\033[35m%s\033[0m")
)

// Color ...
func Color(colorString string) func(...interface{}) string {
	sprint := func(args ...interface{}) string {
		return fmt.Sprintf(colorString,
			fmt.Sprint(args...))
	}
	return sprint
}

// Graylog 的錯誤等級
// const (
// 	levelEmerg   = int8(0)
// 	levelAlert   = int8(1)
// 	levelCrit    = int8(2)
// 	levelErr     = int8(3)
// 	levelWarning = int8(4)
// 	levelNotice  = int8(5)
// 	levelInfo    = int8(6)
// 	levelDebug   = int8(7)
// )

// type severityHook struct{}

// // Run ...
// func (h severityHook) Run(e *zerolog.Event, level zerolog.Level, msg string) {
// 	lvl := int8(0)
// 	switch level {
// 	case zerolog.DebugLevel:
// 		lvl = levelDebug
// 	case zerolog.InfoLevel:
// 		lvl = levelInfo
// 	case zerolog.WarnLevel:
// 		lvl = levelWarning
// 	case zerolog.ErrorLevel:
// 		lvl = levelErr
// 	case zerolog.FatalLevel:
// 		lvl = levelCrit
// 	}
// 	e.Int8("graylog_level", lvl).
// 		Float64("timestamp", float64(time.Now().UnixNano()/int64(time.Millisecond))/1000)
// 	if msg == "" {
// 		e.Str("message", "no message")
// 	}
// }

// Init 初始化 zerolog
func Init(debug bool) {
	zerolog.DisableSampling(true)
	zerolog.TimestampFieldName = "timestamp"
	zerolog.TimeFieldFormat = zerolog.TimeFormatUnix
	hostname, _ := os.Hostname()
	lvl := zerolog.InfoLevel
	if debug {
		lvl = zerolog.DebugLevel
	}
	log.Logger = zerolog.New(os.Stdout).With().
		Str("host", hostname).
		Logger().
		Level(lvl)
}

type Format uint64

const (
	ConsoleFormat Format = 1
	JSONFormat    Format = 2
)

// Config ...
type Config struct {
	Format          Format        `mapstructure:"format"`
	Level           zerolog.Level `mapstructure:"level"`
	AppID           string        `yaml:"app_id" json:"app_id" mapstructure:"app_id"`
	IsSentryEnabled bool          `mapstructure:"is_sentry_enabled"`
	Env             string
}

// NewInjection ...
func (c Config) NewInjection() *Config {
	return &c
}

// New ...
func New(c *Config) {
	zerolog.DisableSampling(true)
	zerolog.TimestampFieldName = "local_timestamp"
	zerolog.TimeFieldFormat = zerolog.TimeFormatUnixMs
	hostname, _ := os.Hostname()
	lvl := zerolog.InfoLevel
	if c.Level == 0 {
		lvl = zerolog.DebugLevel
	} else {
		lvl = c.Level
	}

	var (
		z      zerolog.Logger
		output io.Writer
	)

	if c.Format == ConsoleFormat {
		consoleWriter := zerolog.ConsoleWriter{
			Out: os.Stdout,
		}
		consoleWriter.FormatMessage = func(i interface{}) string {
			return fmt.Sprintf("[ %s ]", i)
		}
		consoleWriter.FormatFieldName = func(i interface{}) string {
			return fmt.Sprintf("%s:", Teal(i))
		}
		consoleWriter.FormatFieldValue = func(i interface{}) string {
			return fmt.Sprintf("%s", i)
		}
		consoleWriter.FormatTimestamp = func(i interface{}) string {
			t := fmt.Sprintf("%s", i)
			millisecond, err := strconv.ParseInt(fmt.Sprintf("%s", i), 10, 64)
			if err == nil {
				t = time.UnixMilli(millisecond).Local().Format("2006/01/02 15:04:05")
			}
			return Yellow(t)
		}
		output = consoleWriter
	}

	if c.IsSentryEnabled {
		multiWriter := zerolog.MultiLevelWriter(new(Writer), os.Stdout)
		z = zerolog.New(multiWriter)
	} else {
		z = zerolog.New(output)
	}

	log.Logger = z.With().
		Fields(map[string]interface{}{
			"app_id": c.AppID,
			"env":    c.Env,
		}).
		Str("host", hostname).
		Timestamp().
		Caller().
		Logger().
		Level(lvl)
}

// Ctx wrap zerolog Ctx func, if ctx not setting logger, return a default prevent for panic
func Ctx(ctx context.Context) *zerolog.Logger {
	defaultLogger := log.Logger
	if ctx == nil {
		defaultLogger.Warn().Msg("zlog func Ctx() not set context.Context in right way.")
		return &defaultLogger
	}

	return log.Ctx(ctx) // if ctx is not null and not set logger yet. A disabled logger is returned.
}
