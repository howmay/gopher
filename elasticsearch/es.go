package elasticsearch

import (
	"context"
	"fmt"
	"time"

	"github.com/cenkalti/backoff/v4"
	"github.com/olivere/elastic/v7"
	"github.com/rs/zerolog/log"
	"github.com/sirupsen/logrus"
)

type Config struct {
	URL      string `mapstructure:"url"`
	Sniff    bool
	Username string
	Password string
}

func New(cfg *Config) (*elastic.Client, error) {
	bo := backoff.NewExponentialBackOff()
	bo.MaxElapsedTime = time.Duration(180) * time.Second

	var client *elastic.Client

	err := backoff.Retry(func() error {
		var err error
		client, err = elastic.NewClient(
			elastic.SetSniff(cfg.Sniff),
			elastic.SetURL(cfg.URL),
			elastic.SetBasicAuth(cfg.Username, cfg.Password),
			elastic.SetTraceLog(logrus.New()),
		)
		if err != nil {
			log.Error().Msgf("new es client fail, err: %s", err.Error())
			return err
		}

		_, _, err = client.Ping(fmt.Sprintf("%s/ping", cfg.URL)).Do(context.Background())
		if err != nil {
			log.Error().Msgf("ping es client fail, err: %s", err.Error())
			return err
		}

		return nil
	}, bo)
	if err != nil {
		return nil, err
	}

	return client, nil
}
