package gin

import (
	"context"
	"errors"
	"net/http"
	_ "net/http/pprof"
	"strings"
	"time"

	"github.com/gin-contrib/pprof"
	"github.com/gin-gonic/gin"
	"github.com/rs/zerolog/log"
	swaggerfiles "github.com/swaggo/files"
	ginswagger "github.com/swaggo/gin-swagger"
	"gitlab.com/howmay/gopher/ctxutil"
	"go.uber.org/fx"
)

type Config struct {
	Mode string `mapstructure:"mode"`
	Port string `mapstructure:"port"`
}

func StartGin(lc fx.Lifecycle, cfg *Config) *gin.Engine {
	ctx := context.Background()
	gin.SetMode(gin.ReleaseMode)
	var e = gin.New()

	server := &http.Server{
		Addr:    ":" + cfg.Port,
		Handler: e,
	}
	lc.Append(fx.Hook{
		OnStart: func(context.Context) error {
			log.Ctx(ctx).Info().Msgf("Starting gin server, listen on %s.", cfg.Port)
			var err error
			go func() {
				if err = server.ListenAndServe(); err != nil && !errors.Is(err, http.ErrServerClosed) {
					log.Error().Msgf("Fail to run gin server, err: %+v", err)
				}
			}()
			return nil
		},
		OnStop: func(context.Context) error {
			log.Ctx(ctx).Info().Msg("Stopping gin HTTP server.")
			c, cancel := context.WithTimeout(context.Background(), 30*time.Second)
			defer cancel()
			return server.Shutdown(c)
		},
	})

	e.Use(func(c *gin.Context) {
		c.Next()
		m := map[string]interface{}{
			"url":        c.Request.URL.String(),
			"method":     c.Request.Method,
			"status":     c.Request.Response,
			"header":     c.Request.Header,
			"client_ip":  c.ClientIP(),
			"access_log": true,
			"trace_id":   ctxutil.TraceIDWithContext(c.Request.Context()),
		}
		if !strings.Contains(c.Request.URL.Path, "graph") && !strings.Contains(c.Request.URL.Path, "health") {
			logger := log.With().Fields(m).Logger()
			logger.Info().Msg("access log")
		}
	})
	e.Use(NewRequestIDMiddleware())
	e.Use(NewRecoverMiddleware())
	e.Use(CORS())

	pprof.Register(e)
	return e
}

func RegisterSwagger(e *gin.Engine) {
	e.GET("/swagger/*any", ginswagger.WrapHandler(swaggerfiles.Handler))
}
