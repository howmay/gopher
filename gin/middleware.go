package gin

import (
	"fmt"
	"runtime"
	"time"

	"github.com/gin-contrib/cors"
	"github.com/gin-gonic/gin"
	"github.com/rs/xid"
	"github.com/sirupsen/logrus"
	"gitlab.com/howmay/gopher/ctxutil"
	"gitlab.com/howmay/gopher/errors"
)

// NewRecoverMiddleware handles panic error
func NewRecoverMiddleware() gin.HandlerFunc {
	return func(c *gin.Context) {
		defer func() {
			if r := recover(); r != nil {
				// unknown error.  handler status code is 500 series.
				trace := make([]byte, 4096)
				runtime.Stack(trace, true)
				traceID := ctxutil.TraceIDWithContext(c.Request.Context())
				customFields := logrus.Fields{
					"url":         c.Request.RequestURI,
					"stack_error": string(trace),
					"trace_id":    traceID,
				}

				var msg string
				for i := 2; ; i++ {
					_, file, line, ok := runtime.Caller(i)
					if !ok {
						break
					}
					msg += fmt.Sprintf("%s:%d\n", file, line)
				}

				logrus.WithFields(customFields).Errorf("http: unknown error: %s", msg)

				httpError := errors.HTTPError{
					Code:    errors.ErrInternalError.Code,
					Message: "unknow error",
				}
				c.AbortWithStatusJSON(500, httpError)
			}
		}()
		c.Next()
	}
}

// NewRequestIDMiddleware Default returns the location middleware with default configuration.
func NewRequestIDMiddleware() gin.HandlerFunc {
	return func(c *gin.Context) {
		requestID := c.Request.Header.Get(ctxutil.XTraceID.String())
		if requestID == "" {
			requestID = xid.New().String()
		}
		ctx := ctxutil.ContextWithXTraceID(c.Request.Context(), requestID)
		ctx = ctxutil.ContextWithXRealIP(ctx, c.ClientIP())
		c.Request = c.Request.WithContext(ctx)

		c.Request.Header.Set(ctxutil.XTraceID.String(), requestID)
		c.Writer.Header().Set(ctxutil.XTraceID.String(), requestID)
		c.Next()
	}
}

func CORS() gin.HandlerFunc {
	return cors.New(cors.Config{
		AllowMethods: []string{"GET", "POST", "PUT", "DELETE", "PATH", "OPTIONS"},
		AllowHeaders: []string{
			HeaderAccept,
			HeaderAcceptEncoding,
			HeaderAuthorization,
			HeaderContentDisposition,
			HeaderContentEncoding,
			HeaderContentLength,
			HeaderContentType,
			HeaderCookie,
			HeaderSetCookie,
			HeaderIfModifiedSince,
			HeaderLastModified,
			HeaderLocation,
			HeaderUpgrade,
			HeaderVary,
			HeaderWWWAuthenticate,
			HeaderXForwardedFor,
			HeaderXForwardedProto,
			HeaderXForwardedProtocol,
			HeaderXForwardedSsl,
			HeaderXUrlScheme,
			HeaderXHTTPMethodOverride,
			HeaderXRealIP,
			HeaderXRequestID,
			HeaderXRequestedWith,
			HeaderServer,
			HeaderOrigin,
			string(ctxutil.XTraceID),
			DeviceID,
		},
		AllowWebSockets: true,
		AllowOriginFunc: func(_ string) bool {
			return true
		},
		AllowBrowserExtensions: true,
		AllowCredentials:       true,
		MaxAge:                 24 * time.Hour,
	})
}

const (
	HeaderAccept              = "Accept"
	HeaderAcceptEncoding      = "Accept-Encoding"
	HeaderAllow               = "Allow"
	HeaderAuthorization       = "Authorization"
	HeaderContentDisposition  = "Content-Disposition"
	HeaderContentEncoding     = "Content-Encoding"
	HeaderContentLength       = "Content-Length"
	HeaderContentType         = "Content-Type"
	HeaderCookie              = "Cookie"
	HeaderSetCookie           = "Set-Cookie"
	HeaderIfModifiedSince     = "If-Modified-Since"
	HeaderLastModified        = "Last-Modified"
	HeaderLocation            = "Location"
	HeaderUpgrade             = "Upgrade"
	HeaderVary                = "Vary"
	HeaderWWWAuthenticate     = "WWW-Authenticate"
	HeaderXForwardedFor       = "X-Forwarded-For"
	HeaderXForwardedProto     = "X-Forwarded-Proto"
	HeaderXForwardedProtocol  = "X-Forwarded-Protocol"
	HeaderXForwardedSsl       = "X-Forwarded-Ssl"
	HeaderXUrlScheme          = "X-Url-Scheme"
	HeaderXHTTPMethodOverride = "X-HTTP-Method-Override"
	HeaderXRealIP             = "X-Real-IP"
	HeaderXRequestID          = "X-Request-ID"
	HeaderXRequestedWith      = "X-Requested-With"
	HeaderServer              = "Server"
	HeaderOrigin              = "Origin"

	DeviceID = "X-Device-ID"
)
