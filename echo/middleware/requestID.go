package middleware

import (
	"context"

	"github.com/labstack/echo/v4"
	"github.com/rs/xid"
	"github.com/rs/zerolog/log"
	"gitlab.com/howmay/gopher/ctxutil"
)

// CtxKey 用來代表 context.Context 的 key
type CtxKey string

func (ck CtxKey) String() string {
	return string(ck)
}

// RequestIDFromContext 從 ctx 中取得 request id, 如果沒有即時產生一個
func RequestIDFromContext(ctx context.Context) string {
	rid, ok := ctx.Value(echo.HeaderXRequestID).(string)
	if !ok {
		// 產生 requestID 並傳下去
		rid = xid.New().String()
		return rid
	}
	return rid
}

// NewRequestIDMiddleware Default returns the location middleware with default configuration.
func NewRequestIDMiddleware() echo.MiddlewareFunc {
	return func(next echo.HandlerFunc) echo.HandlerFunc {
		return func(c echo.Context) (err error) {
			requestID := c.Request().Header.Get(string(ctxutil.XTraceID))
			if requestID == "" {
				requestID = xid.New().String()
			}
			ctx := context.WithValue(c.Request().Context(), ctxutil.XTraceID, requestID)
			logger := log.With().Str("trace_id", requestID).Logger()

			ctx = logger.WithContext(ctx)

			c.SetRequest(c.Request().WithContext(ctx))
			// Set X-Request-Id header
			c.Response().Writer.Header().Set(string(ctxutil.XTraceID), requestID)

			return next(c)
		}
	}
}
