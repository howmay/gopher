package middleware

import (
	"bufio"
	"bytes"
	"io"
	"net"
	"net/http"
	"strings"

	"github.com/labstack/echo/v4"
	"github.com/rs/zerolog/log"
)

type bodyDumpResponseWriter struct {
	io.Writer
	http.ResponseWriter
}

// RequestDump output http request dump
func RequestDump(ignorePath ...string) echo.MiddlewareFunc {
	var ignorePathMap map[string]struct{}
	if len(ignorePath) != 0 {
		ignorePathMap = make(map[string]struct{})
		for _, path := range ignorePath {
			ignorePathMap[path] = struct{}{}
		}
	}

	return func(next echo.HandlerFunc) echo.HandlerFunc {
		return func(c echo.Context) (err error) {
			logger := log.Ctx(c.Request().Context())
			req := c.Request()

			var bReq bytes.Buffer
			if req.Body != nil {
				var dest io.Writer = &bReq
				_, err = io.Copy(dest, req.Body)
				if err == nil {
					req.Body = io.NopCloser(bytes.NewReader(bReq.Bytes()))
				}
			}

			resp := c.Response()
			resBody := new(bytes.Buffer)
			mw := io.MultiWriter(c.Response().Writer, resBody)
			writer := &bodyDumpResponseWriter{Writer: mw, ResponseWriter: c.Response().Writer}
			c.Response().Writer = writer

			resp.After(func() {
				if resBody.Len() > 500 {
					resBody.Truncate(500)
				}

				for path := range ignorePathMap {
					if strings.Contains(req.URL.Path, path) {
						return
					}
				}
				zlogCtx := logger.With().Str("method", req.Method).Str("path", req.URL.Path)
				if bReq.Len() != 0 {
					zlogCtx = zlogCtx.Str("req_body", bReq.String())
				}
				if resBody.Len() != 0 {
					zlogCtx = zlogCtx.Str("resp_body", resBody.String())
				}
				logger := zlogCtx.Logger()
				if resp.Status >= http.StatusInternalServerError {
					logger.Error().Msg("access log")
				} else if resp.Status >= http.StatusBadRequest {
					logger.Warn().Msg("access log")
				} else {
					logger.Info().Msg("access log")
				}
			})
			return next(c)
		}
	}
}

func (w *bodyDumpResponseWriter) WriteHeader(code int) {
	w.ResponseWriter.WriteHeader(code)
}

func (w *bodyDumpResponseWriter) Write(b []byte) (int, error) {
	return w.Writer.Write(b)
}

func (w *bodyDumpResponseWriter) Flush() {
	w.ResponseWriter.(http.Flusher).Flush()
}

func (w *bodyDumpResponseWriter) Hijack() (net.Conn, *bufio.ReadWriter, error) {
	return w.ResponseWriter.(http.Hijacker).Hijack()
}
