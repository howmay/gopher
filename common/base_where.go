package common

import (
	"fmt"
	"time"

	"gorm.io/gorm"
)

func (where *BaseWhere) Where(db *gorm.DB) *gorm.DB {
	if where.Ids != nil && len(where.Ids) != 0 {
		db = db.Where("id IN (?)", where.Ids)
	}
	if len(where.SearchIn) != 0 && where.Keyword != "" {
		tmpDB := db
		for _, field := range where.SearchIn {
			field := field
			tmpDB = tmpDB.Or(fmt.Sprintf("%s LIKE ?", field), "%"+where.Keyword+"%")
		}
		db = db.Where(tmpDB)
	}

	if createdAtLt := time.Unix(where.CreatedAtLt, 0); !createdAtLt.IsZero() {
		db = db.Where("created_at < ?", createdAtLt)
	}

	if createdAtLte := time.Unix(where.CreatedAtLte, 0); !createdAtLte.IsZero() {
		db = db.Where("created_at <= ?", createdAtLte)
	}

	if createdAtGt := time.Unix(where.CreatedAtGt, 0); !createdAtGt.IsZero() {
		db = db.Where("created_at > ?", createdAtGt)
	}

	if createdAtGte := time.Unix(where.CreatedAtGte, 0); !createdAtGte.IsZero() {
		db = db.Where("created_at >= ?", createdAtGte)
	}

	if updatedAtLt := time.Unix(where.UpdatedAtLt, 0); !updatedAtLt.IsZero() {
		db = db.Where("updated_at < ?", updatedAtLt)
	}

	if updatedAtLte := time.Unix(where.UpdatedAtLte, 0); !updatedAtLte.IsZero() {
		db = db.Where("updated_at <= ?", updatedAtLte)
	}

	if updatedAtGt := time.Unix(where.UpdatedAtGt, 0); !updatedAtGt.IsZero() {
		db = db.Where("updated_at > ?", updatedAtGt)
	}

	if updatedAtGte := time.Unix(where.UpdatedAtGte, 0); !updatedAtGte.IsZero() {
		db = db.Where("updated_at >= ?", updatedAtGte)
	}

	if where.CreatorId != 0 {
		db = db.Where("creator_id = ?", where.UpdaterId)
	}
	if where.CreatorName != "" {
		db = db.Where("creator_name = ?", where.CreatorName)
	}
	if where.UpdaterId != 0 {
		db = db.Where("updater_id = ?", where.UpdaterId)
	}
	if where.UpdaterName != "" {
		db = db.Where("updater_name = ?", where.UpdaterName)
	}

	if deletedAtLt := time.Unix(where.DeletedAtLt, 0); !deletedAtLt.IsZero() {
		db = db.Where("deleted_at < ?", deletedAtLt)
	}

	if deletedAtLte := time.Unix(where.DeletedAtLte, 0); !deletedAtLte.IsZero() {
		db = db.Where("deleted_at <= ?", deletedAtLte)
	}

	if deletedAtGt := time.Unix(where.DeletedAtGt, 0); !deletedAtGt.IsZero() {
		db = db.Where("deleted_at > ?", deletedAtGt)
	}

	if deletedAtGte := time.Unix(where.DeletedAtGte, 0); !deletedAtGte.IsZero() {
		db = db.Where("deleted_at >= ?", deletedAtGte)
	}
	return db
}
