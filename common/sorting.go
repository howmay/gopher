package common

import (
	"fmt"

	"gorm.io/gorm"
)

// Sort 依單一欄位單一方向排序
func (s *Sorting) Sort(db *gorm.DB) *gorm.DB {
	if len(s.SortField) != 0 && s.Type != 0 {
		var sortOrder = "DESC"
		if s.Type == SortingOrderType_ASC {
			sortOrder = "ASC"
		}
		db = db.Order(fmt.Sprintf("%s %s", s.SortField, sortOrder))
	}
	return db
}
