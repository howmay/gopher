package grpc

import (
	"context"
	"net"
	"runtime"
	"time"

	"github.com/rs/zerolog/log"
	"gitlab.com/howmay/gopher/errors"
	"gitlab.com/howmay/gopher/grpc/grpc_error"
	"gitlab.com/howmay/gopher/grpc/grpc_zlog"
	"gitlab.com/howmay/gopher/grpc/trace_id"
	"go.uber.org/fx"
	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials/insecure"
	"google.golang.org/grpc/keepalive"
	"google.golang.org/grpc/reflection"
)

// Config for grpc config
type Config struct {
	Debug   bool
	Address string
}

// NewInjection ...
func (s *Config) NewInjection() *Config {
	return s
}

// NewTCPServer ...
func NewTCPServer(cfg *Config) (net.Listener, error) {
	lis, err := net.Listen("tcp", cfg.Address)
	if err != nil {
		log.Ctx(context.Background()).Error().Msgf("main: bind identity grpc failed: %v", err)
	}
	return lis, err
}

type NewGrpcParams struct {
	fx.In

	UnaryInterceptor  []grpc.UnaryServerInterceptor  `group:"unary_interceptor"`
	StreamInterceptor []grpc.StreamServerInterceptor `group:"stream_interceptor"`
}

// NewGRPCServer ...
func NewGRPCServer(p NewGrpcParams) *grpc.Server {
	var (
		chain                  grpc.ServerOption
		streamChain            grpc.ServerOption
		grpcUnaryInterceptors  = make([]grpc.UnaryServerInterceptor, 0)
		grpcStreamInterceptors = make([]grpc.StreamServerInterceptor, 0)
	)

	grpcUnaryInterceptors = append(
		grpcUnaryInterceptors,
		trace_id.UnaryServerXTraceIDInterceptor(),
		grpc_error.UnaryServerConvertGrpcErrorInterceptor(),
		grpc_zlog.UnaryServerZLogInterceptor(log.Logger),
	)
	grpcStreamInterceptors = append(
		grpcStreamInterceptors,
		trace_id.StreamServerXTraceInterceptor(),
		grpc_error.StreamServerConvertGrpcErrorInterceptor(),
		grpc_zlog.StreamServerInterceptor(log.Logger),
	)

	if len(p.UnaryInterceptor) != 0 {
		grpcUnaryInterceptors = append(grpcUnaryInterceptors, p.UnaryInterceptor...)
	}
	if len(p.StreamInterceptor) != 0 {
		grpcStreamInterceptors = append(grpcStreamInterceptors, p.StreamInterceptor...)
	}

	chain = grpc.ChainUnaryInterceptor(grpcUnaryInterceptors...)
	streamChain = grpc.ChainStreamInterceptor(grpcStreamInterceptors...)

	s := grpc.NewServer(
		grpc.KeepaliveParams(
			keepalive.ServerParameters{
				Time:              time.Duration(5) * time.Second, // Ping the client if it is idle for 5 seconds to ensure the connection is still active
				Timeout:           time.Duration(5) * time.Second, // Wait 5 second for the ping ack before assuming the connection is dead
				MaxConnectionIdle: 5 * time.Minute,
			},
		),
		grpc.KeepaliveEnforcementPolicy(
			keepalive.EnforcementPolicy{
				MinTime:             time.Duration(2) * time.Second, // If a client pings more than once every 2 seconds, terminate the connection
				PermitWithoutStream: true,                           // Allow pings even when there are no active streams
			},
		),
		chain,
		streamChain,
	)

	return s
}

// StartServer ...
func StartServer(lc fx.Lifecycle, cfg *Config, p NewGrpcParams) (*grpc.Server, error) {
	lis, err := NewTCPServer(cfg)
	if err != nil {
		return nil, err
	}

	s := NewGRPCServer(p)

	lc.Append(fx.Hook{
		OnStart: func(ctx context.Context) error {
			runtime.SetBlockProfileRate(1)
			go func() {
				log.Ctx(ctx).Info().Msgf("Starting grpc server, listen on %s", lis.Addr().String())
				reflection.Register(s)
				if err := s.Serve(lis); err != nil {
					log.Ctx(ctx).Error().Msgf("main: failed to start grpc server: %v", err)
				}
			}()
			return nil
		},
		OnStop: func(ctx context.Context) error {
			log.Ctx(ctx).Info().Msgf("Stopping grpc server.")
			s.GracefulStop()
			err := lis.Close()
			if err != nil && !errors.Is(err, net.ErrClosed) {
				return err
			}
			return nil
		},
	})
	return s, nil
}

// NewClient ... new grpc client
func NewClient(host string, callOpts ...grpc.CallOption) (*grpc.ClientConn, error) {
	if len(callOpts) == 0 {
		callOpts = append(callOpts, grpc.MaxCallRecvMsgSize(10*1024*1024)) // set max message size to 10MB
	}

	conn, err := grpc.NewClient(host, grpc.WithTransportCredentials(insecure.NewCredentials()),
		grpc.WithDefaultCallOptions(
			callOpts...,
		),
		grpc.WithInitialConnWindowSize(256*1024),
		grpc.WithKeepaliveParams(keepalive.ClientParameters{
			Time:                20 * time.Second,
			Timeout:             18 * time.Second,
			PermitWithoutStream: true,
		}),
		grpc.WithChainUnaryInterceptor(
			trace_id.UnaryClientXRequestIDInterceptor(),
			grpc_error.UnaryClientConvertHTTPErrInterceptor(),
		),
	)
	return conn, err
}
