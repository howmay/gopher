package grpc_error

import (
	"context"

	"gitlab.com/howmay/gopher/errors"
	"gitlab.com/howmay/gopher/helper"
	"google.golang.org/grpc"
)

// UnaryServerConvertGrpcErrorInterceptor 將自訂的 error 轉成 grpc status error
func UnaryServerConvertGrpcErrorInterceptor() grpc.UnaryServerInterceptor {
	return func(ctx context.Context, req interface{}, _ *grpc.UnaryServerInfo, handler grpc.UnaryHandler) (_ interface{}, err error) {
		defer helper.Recover(ctx)
		resp, err := handler(ctx, req)
		if err != nil {
			return resp, errors.ConvertProtoErr(ctx, err)
		}
		return resp, err
	}
}

// StreamServerConvertGrpcErrorInterceptor 將自訂的 error 轉成 grpc status error
func StreamServerConvertGrpcErrorInterceptor() grpc.StreamServerInterceptor {
	return func(srv interface{}, ss grpc.ServerStream, _ *grpc.StreamServerInfo, handler grpc.StreamHandler) error {
		defer helper.Recover(ss.Context())
		err := handler(srv, ss)
		if err != nil {
			return errors.ConvertProtoErr(ss.Context(), err)
		}
		return err
	}
}

// UnaryClientConvertHTTPErrInterceptor 將 grpc status error 轉成內部 error 結構
func UnaryClientConvertHTTPErrInterceptor() grpc.UnaryClientInterceptor {
	return func(ctx context.Context, method string, req, reply interface{}, cc *grpc.ClientConn, invoker grpc.UnaryInvoker, opts ...grpc.CallOption) error {
		err := invoker(ctx, method, req, reply, cc, opts...)
		return errors.ConvertHTTPErr(err)
	}
}
