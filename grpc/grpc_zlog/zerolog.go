package grpc_zlog

import (
	"context"
	"path"
	"time"

	grpc_middleware "github.com/grpc-ecosystem/go-grpc-middleware"

	"github.com/rs/zerolog"
	"github.com/rs/zerolog/log"
	"gitlab.com/howmay/gopher/ctxutil"
	"gitlab.com/howmay/gopher/helper"
	"google.golang.org/grpc"
	"google.golang.org/grpc/metadata"
	"google.golang.org/grpc/status"
)

var defaultOptions = &options{
	isLogRequest:  true,
	isLogResponse: true,
}

type options struct {
	isLogRequest  bool
	isLogResponse bool
}

// Option zerolog interceptor option
type Option func(*options)

// UnaryServerZLogInterceptor ...
func UnaryServerZLogInterceptor(logger zerolog.Logger, opts ...Option) grpc.UnaryServerInterceptor {
	o := evaluateServerOpt(opts)
	return func(ctx context.Context, req interface{}, info *grpc.UnaryServerInfo, handler grpc.UnaryHandler) (_ interface{}, err error) {
		startTime := time.Now()
		newCtx := newLoggerForCall(ctx, logger, info.FullMethod, startTime)

		resp, err := handler(newCtx, req)

		duration := time.Since(startTime)
		l := log.Ctx(newCtx).With().Float32("grpc.time_ms", durationToMilliseconds(duration)).Logger()
		if err != nil {
			levt := l.Error().Interface("grpc.req", req)
			s, ok := status.FromError(err)
			if ok {
				levt.Err(err).
					Uint32("grpc.status_code", uint32(s.Code())).
					Str("grpc.status_msg", s.Message()).
					Msgf("%+v", err)
			} else {
				levt.Err(err).Msgf("%+v", err)
			}
		} else {
			levt := l.Info()
			if o.isLogRequest {
				levt = levt.Interface("grpc.req", req)
			}
			if o.isLogResponse {
				levt = levt.Interface("grpc.resp", resp)
			}
			levt.Msg("access log")
		}

		return resp, err
	}
}

func newLoggerForCall(ctx context.Context, logger zerolog.Logger, fullMethodString string, start time.Time) context.Context {
	service := path.Dir(fullMethodString)[1:]
	method := path.Base(fullMethodString)

	callLog := logger.With().
		Time("grpc.start_time", start).
		Str("grpc.service", service).
		Str("grpc.method", method).
		Str("tracie_id", ctxutil.MetadataFromContext(ctx)).
		Logger()
	return callLog.WithContext(ctx)
}

func durationToMilliseconds(duration time.Duration) float32 {
	return float32(duration.Nanoseconds()/1000) / 1000
}

func evaluateServerOpt(opts []Option) *options {
	optCopy := &options{}
	*optCopy = *defaultOptions
	for _, o := range opts {
		if o != nil {
			o(optCopy)
		}
	}
	return optCopy
}

// StreamServerInterceptor returns a new streaming server interceptor that adds logrus.Entry to the context.
func StreamServerInterceptor(logger zerolog.Logger, opts ...Option) grpc.StreamServerInterceptor {
	return func(srv interface{}, stream grpc.ServerStream, info *grpc.StreamServerInfo, handler grpc.StreamHandler) error {
		startTime := time.Now()
		newCtx := newLoggerForCall(stream.Context(), logger, info.FullMethod, startTime)
		wrapped := grpc_middleware.WrapServerStream(stream)
		wrapped.WrappedContext = newCtx
		md, _ := metadata.FromIncomingContext(wrapped.WrappedContext)
		defer helper.Recover(newCtx)
		l := log.Ctx(newCtx).With().Logger()
		l = l.With().
			Interface("grpc.meta_data", md).
			Bool("grpc.is_stream", true).Logger()

		if err := handler(srv, wrapped); err != nil {
			levt := l.Error()
			s, ok := status.FromError(err)
			if ok {
				levt.Err(err).
					Uint32("grpc.status_code", uint32(s.Code())).
					Str("grpc.status_msg", s.Message()).
					Msgf("%+v", err)
			} else {
				levt.Err(err).Msgf("%+v", err)
			}
			return err
		}
		l.Info().Msg("access log")

		return nil
	}
}
