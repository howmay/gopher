package trace_id

import (
	"context"

	"gitlab.com/howmay/gopher/ctxutil"
	"gitlab.com/howmay/gopher/grpc/stream"
	"google.golang.org/grpc"
	"google.golang.org/grpc/metadata"
)

// UnaryServerXTraceInterceptor 從 metadata 拿 x-trace-id 並放到 ctx 裡
// 如果 metadata 沒有 x-trace-id 則產生 trace id 並放進 ctx
func UnaryServerXTraceIDInterceptor() grpc.UnaryServerInterceptor {
	return func(ctx context.Context, req interface{}, _ *grpc.UnaryServerInfo, handler grpc.UnaryHandler) (_ interface{}, err error) {
		traceID := ctxutil.MetadataFromContext(ctx)
		ctxutil.MetadataXTraceID(ctx, traceID)
		ctx = ctxutil.ContextWithXTraceID(ctx, traceID)
		header := metadata.Pairs("trace_id", traceID)
		grpc.SendHeader(ctx, header)

		resp, err := handler(ctx, req)
		return resp, err
	}
}

// StreamServerXTraceInterceptor 從 metadata 拿 x-trace-id 並放到 ctx 裡
// 如果 metadata 沒有 x-trace-id 則產生 trace id 並放進 ctx
func StreamServerXTraceInterceptor() grpc.StreamServerInterceptor {
	return func(srv interface{}, ss grpc.ServerStream, _ *grpc.StreamServerInfo, handler grpc.StreamHandler) error {
		wrappedStream := stream.WrapServerStream(ss)
		ctx := ss.Context()
		traceID := ctxutil.MetadataFromContext(ctx)
		ctxutil.MetadataXTraceID(ctx, traceID)
		header := metadata.Pairs("trace_id", traceID)
		grpc.SendHeader(ctx, header)

		wrappedStream.WrappedContext = ctxutil.ContextWithXTraceID(ctx, traceID)
		return handler(srv, wrappedStream)
	}
}
