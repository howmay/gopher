package trace_id

import (
	"context"

	"gitlab.com/howmay/gopher/ctxutil"
	"google.golang.org/grpc"
)

// UnaryClientXRealIPInterceptor 將 x-request-id 設定到 grpc metadata
func UnaryClientXRequestIDInterceptor() grpc.UnaryClientInterceptor {
	return func(ctx context.Context, method string, req, reply interface{}, cc *grpc.ClientConn, invoker grpc.UnaryInvoker, opts ...grpc.CallOption) error {
		err := invoker(ctxutil.MetadataXTraceID(ctx, ctxutil.GetTraceIDFromContext(ctx)), method, req, reply, cc, opts...)
		return err
	}
}
