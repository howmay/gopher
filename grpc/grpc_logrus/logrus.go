package grpc_logrus

import (
	"context"
	"fmt"
	"path"
	"runtime"
	"time"

	grpc_middleware "github.com/grpc-ecosystem/go-grpc-middleware"
	"github.com/grpc-ecosystem/go-grpc-middleware/logging/logrus/ctxlogrus"
	"github.com/sirupsen/logrus"
	"gitlab.com/howmay/gopher/ctxutil"
	"gitlab.com/howmay/gopher/errors"
	"gitlab.com/howmay/gopher/helper"
	"google.golang.org/grpc"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/metadata"
)

var (
	// SystemField is used in every log statement made through grpc_logrus. Can be overwritten before any initialization code.
	SystemField = "system"

	// KindField describes the log field used to indicate whether this is a server or a client log statement.
	KindField = "span.kind"
)

// UnaryServerInterceptor returns a new unary server interceptors that adds logrus.Entry to the context.
func UnaryServerInterceptor(entry *logrus.Entry, opts ...Option) grpc.UnaryServerInterceptor {
	o := evaluateServerOpt(opts)
	return func(ctx context.Context, req interface{}, info *grpc.UnaryServerInfo, handler grpc.UnaryHandler) (interface{}, error) {
		startTime := time.Now()

		fields := logrus.Fields{
			"grpc.req":   req,
			"access_log": true,
		}
		md, ok := metadata.FromIncomingContext(ctx)
		if ok {
			fields["meta_data"] = md
		}

		newCtx := newLoggerForCall(ctx, entry, info.FullMethod, startTime)

		defer func() {
			if r := recover(); r != nil {
				var msg string
				for i := 2; ; i++ {
					_, file, line, ok := runtime.Caller(i)
					if !ok {
						break
					}
					msg += fmt.Sprintf("%s:%d\n", file, line)
				}
				msg = fmt.Sprintf("%s\n↧↧↧↧↧↧ PANIC ↧↧↧↧↧↧\n%s↥↥↥↥↥↥ PANIC ↥↥↥↥↥↥", r, msg)
				o.messageFunc(newCtx, "panic error: \n"+msg, logrus.FatalLevel, codes.Internal, errors.ErrInternalError, fields)
			}
		}()

		resp, err := handler(newCtx, req)
		if !o.shouldLog(info.FullMethod, err) {
			return resp, err
		}
		durField, durVal := o.durationFunc(time.Since(startTime))
		code := o.codeFunc(err)
		level := o.levelFunc(code)
		fields["grpc.code"] = code.String()
		fields[durField] = durVal

		if err != nil {
			fields[logrus.ErrorKey] = err
		}
		if level < logrus.WarnLevel {
			fields["stack_trace"] = fmt.Sprintf("%+v", err)
		}

		o.messageFunc(newCtx, "access log", level, code, err, fields)
		return resp, err
	}
}

// StreamServerInterceptor returns a new streaming server interceptor that adds logrus.Entry to the context.
func StreamServerInterceptor(entry *logrus.Entry, opts ...Option) grpc.StreamServerInterceptor {
	o := evaluateServerOpt(opts)
	return func(srv interface{}, stream grpc.ServerStream, info *grpc.StreamServerInfo, handler grpc.StreamHandler) error {
		startTime := time.Now()
		newCtx := newLoggerForCall(stream.Context(), entry, info.FullMethod, startTime)
		wrapped := grpc_middleware.WrapServerStream(stream)
		wrapped.WrappedContext = newCtx
		md, _ := metadata.FromIncomingContext(wrapped.WrappedContext)
		fields := logrus.Fields{
			"access_log": true,
			"is_stream":  true,
			"meta_data":  md,
		}
		defer helper.Recover(newCtx)
		o.messageFunc(newCtx, "access log", logrus.InfoLevel, codes.OK, nil, fields)

		err := handler(srv, wrapped)

		if !o.shouldLog(info.FullMethod, err) {
			return err
		}
		code := o.codeFunc(err)
		level := o.levelFunc(code)
		durField, durVal := o.durationFunc(time.Since(startTime))
		fields = logrus.Fields{}
		fields[durField] = durVal
		if err != nil {
			fields[logrus.ErrorKey] = err
		}
		if level < logrus.WarnLevel {
			fields["stack_trace"] = fmt.Sprintf("%+v", err)
		}
		o.messageFunc(newCtx, "response log", level, code, err, fields)
		return err
	}
}

func newLoggerForCall(ctx context.Context, entry *logrus.Entry, fullMethodString string, start time.Time) context.Context {
	service := path.Dir(fullMethodString)[1:]
	method := path.Base(fullMethodString)
	callLog := entry.WithFields(
		logrus.Fields{
			SystemField:       "grpc",
			KindField:         "server",
			"grpc.service":    service,
			"grpc.method":     method,
			"grpc.start_time": start.Format(time.RFC3339),
			"trace_id":        ctxutil.TraceIDWithContext(ctx),
		})

	if d, ok := ctx.Deadline(); ok {
		callLog = callLog.WithFields(
			logrus.Fields{
				"grpc.request.deadline": d.Format(time.RFC3339),
			})
	}

	callLog = callLog.WithFields(ctxlogrus.Extract(ctx).Data)
	return ctxlogrus.ToContext(ctx, callLog)
}
