package ctxutil

import (
	"context"

	"github.com/google/uuid"
	"github.com/rs/xid"
	"github.com/rs/zerolog"
	"github.com/rs/zerolog/log"
	"google.golang.org/grpc/metadata"
)

type ctxKey string

const (
	// XTraceID request id
	XTraceID            ctxKey = "x-trace-id"
	metadataXTraceIDKey string = "x-trace-id"

	XRealIP            ctxKey = "x-real-ip"
	metadataXRealIPKey string = "x-real-ip"

	XDeviceID            ctxKey = "x-device-id"
	metadataXDeviceIDKey string = "x-device-id"
)

func (ctxKey) String() string {
	return "x-trace-id"
}

// GetTraceIDFromContext get trace-id from context
func GetTraceIDFromContext(ctx context.Context) string {
	v, ok := ctx.Value(XTraceID).(string)
	if !ok {
		return uuid.New().String()
	}
	return v
}

func TraceIDWithContext(ctx context.Context) context.Context {
	traceID := GetTraceIDFromContext(ctx)
	if traceID == "" {
		traceID = NewTraceID()
	}

	return ContextWithXTraceID(ctx, traceID)
}

// ContextWithXTraceID returns a context.Context with given trace-id value.
func ContextWithXTraceID(ctx context.Context, traceID string) context.Context {
	return context.WithValue(ctx, XTraceID, traceID)
}

// MetadataXTraceID returns a context.Context with given trace-id value.
func MetadataXTraceID(ctx context.Context, requestID string) context.Context {
	return metadata.AppendToOutgoingContext(ctx, metadataXTraceIDKey, requestID)
}

// GetTraceIDFromContext get trace-id from context
func MetadataFromContext(ctx context.Context) string {
	var requestID string
	md, ok := metadata.FromIncomingContext(ctx)
	if !ok {
		return NewTraceID()
	}
	requestIDMeta, ok := md[metadataXTraceIDKey]
	if !ok || len(requestIDMeta) == 0 {
		requestID = NewTraceID()
	} else {
		requestID = requestIDMeta[0]
	}

	return requestID
}

func NewTraceID() string {
	return xid.New().String()
}

// GetRealIPFromContext get trace-id from context
func GetRealIPFromContext(ctx context.Context) string {
	v, ok := ctx.Value(XRealIP).(string)
	if !ok {
		return ""
	}
	return v
}

// ContextWithXRealIP returns a context.Context with given trace-id value.
func ContextWithXRealIP(ctx context.Context, readIP string) context.Context {
	return context.WithValue(ctx, XRealIP, readIP)
}

// MetadataXRealIP returns a context.Context with given real-ip value.
func MetadataXRealIP(ctx context.Context, readIP string) context.Context {
	return metadata.AppendToOutgoingContext(ctx, metadataXRealIPKey, readIP)
}

// GetDeviceIDFromContext get device-id from context
func GetDeviceIDFromContext(ctx context.Context) string {
	v, ok := ctx.Value(XDeviceID).(string)
	if !ok {
		return ""
	}
	return v
}

// ContextWithXDeviceID returns a context.Context with given device-id value.
func ContextWithXDeviceID(ctx context.Context, deviceID string) context.Context {
	return context.WithValue(ctx, XDeviceID, deviceID)
}

// MetadataXDeviceID returns a context.Context with given device-id value.
func MetadataXDeviceID(ctx context.Context, readIP string) context.Context {
	return metadata.AppendToOutgoingContext(ctx, metadataXDeviceIDKey, readIP)
}

// Extract takes the call-scoped Logger from zerolog interceptor.
func ExtractToZlog(ctx context.Context) zerolog.Logger {
	return *(log.Ctx(ctx))
}
