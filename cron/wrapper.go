package cron

import (
	"context"
	"fmt"
	"runtime"
	"strconv"
	"time"

	"github.com/google/uuid"
	"github.com/rs/zerolog/log"
	"gitlab.com/howmay/gopher/ctxutil"
)

// JobWrapper job wrapper
type JobWrapper func(job Job) Job

// LogWrapper 在執行的前後印出 log
func LogWrapper(key, spec string) JobWrapper {
	return func(job Job) Job {
		return func(ctx context.Context) {
			startAt := time.Now().UnixNano()
			requestID := uuid.New().String()
			logger := log.With().Fields(map[string]interface{}{
				"start_time": startAt,
				"request_id": requestID,
				"jobName":    key,
				"spec":       spec,
			}).Logger()
			logger.Debug().Msgf("schedule %s now is working, and spec = %s", key, spec)
			ctx = logger.WithContext(ctx)
			ctx = ctxutil.ContextWithXTraceID(ctx, requestID)
			ctx = ctxutil.MetadataXTraceID(ctx, requestID)
			// 執行排程工作
			job(ctx)
			spentTime := (time.Now().UnixNano() - startAt) / 1e6
			logger.Debug().Msgf("schedule %s now has done, it spent %d (ms) and spec = %s", key, spentTime, spec)
		}
	}
}

// LogTimeoutWrapper 當執行太久的時候發出通知跟 log
func LogTimeoutWrapper(key string, timeout time.Duration) JobWrapper {
	return func(job Job) Job {
		return func(ctx context.Context) {
			// 超時未結束的話要跳通知
			done := make(chan struct{})
			timeoutTimer := time.NewTicker(timeout)
			go func() {
				select {
				case <-timeoutTimer.C:
					log.Ctx(ctx).Warn().Msgf("scheduler %s timout!!", key)
				case <-done:
					//
				}
			}()

			// 執行排程工作
			job(ctx)
			done <- struct{}{}
		}
	}
}

// SkipIfStillRunningWrapper 重新實作 cron.SkipIfStillRunning
// skips an invocation of the Job if a previous invocation is
// still running. It logs skips to the given logger at Info level.
func SkipIfStillRunningWrapper(key string) JobWrapper {
	return func(job Job) Job {
		var ch = make(chan struct{}, 1)
		ch <- struct{}{}

		return func(ctx context.Context) {
			select {
			case v := <-ch:
				defer func() {
					ch <- v
				}()

				job(ctx)
			default:
				log.Warn().Msgf("scheduler %s skip!!", key)
			}
		}
	}
}

// RecoverJobWrapper recovery job panic
//
//	印出 stack 資訊
func RecoverJobWrapper(key string) JobWrapper {
	return func(job Job) Job {

		return func(ctx context.Context) {
			startTime := time.Now()

			defer func() {
				if err := recover(); err != nil {
					endTime := time.Now()
					trace := make([]byte, 4096)
					runtime.Stack(trace, true)
					var msg string
					for i := 2; ; i++ {
						_, file, line, ok := runtime.Caller(i)
						if !ok {
							break
						}
						msg += fmt.Sprintf("%s:%d\n", file, line)
					}

					log.Error().Stack().Err(err.(error)).Fields(
						map[string]interface{}{
							"stack_error":   string(trace),
							"end_time":      endTime.String(),
							"latency":       strconv.FormatInt(int64(endTime.Sub(startTime)), 10),
							"latency_human": endTime.Sub(startTime).String(),
						}).Msgf("%s\n↧↧↧↧↧↧ PANIC ↧↧↧↧↧↧\n%s↥↥↥↥↥↥ PANIC ↥↥↥↥↥↥\n %s scheduler error", err, msg, key)
				}
			}()
			job(ctx)
		}
	}
}
